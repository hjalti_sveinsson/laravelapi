<?php

use Illuminate\Database\Seeder;
use App\JobsComment;
use App\User;
use App\Job;


use Faker\Factory as Faker;

class JobsCommentTableSeeder extends Seeder
{
    public function run()
    {

      $faker = Faker::create();

      $usersIds = user::lists('id')->toArray();   // returns an array of all ids in users table
      $jobsIds  =  job::lists('id')->toArray();
 
      foreach(range(1,30)as $index)
      {
        JobsComment::create([
          'jobID'     => $faker->randomElement($jobsIds),
          'userid'    => $faker->randomElement($usersIds),
          'comment'   => $faker->paragraph(1)
          ]);
      }
    }
}

