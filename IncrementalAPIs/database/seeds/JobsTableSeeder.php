<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Job;

use Faker\Factory as Faker;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class JobsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $statuslist = ['Hafið','Bið','Klára','Rukka','Vantar KT','Reikn email','Krafa','Greitt'];

        foreach (range(1,30) as $index) 
        {
          Job::create([
            'job'             =>$faker->streetAddress,
            'responsible'     =>$faker->name,
            'price'           =>$faker->randomDigit,
            'status'          =>$faker->randomElement($statuslist),
            'commentFinance'  =>$faker->sentence(1),
            'active'          =>$faker->boolean(75)
            ]);
        }
    }
}

