<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

//use Faker\Factory as Faker;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
//        $faker = Faker::create();

        $hash1 = password_hash('password', PASSWORD_BCRYPT);
        $hash2 = bcrypt('password');

        User::create([
          'name'    =>'Hjalti',
          'email'   =>'hjalti@example.is',
          'password'=>$hash2
          ]);

        User::create([
          'name'    =>'Gummi',
          'email'   =>'gummi@example.is',
          'password'=>$hash2
          ]);

    }
}

