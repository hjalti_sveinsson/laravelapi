<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Job;
use App\user;
use App\JobsComment;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

            job::truncate();
            user::truncate();
            jobscomment::truncate();

            $this->call('JobsTableSeeder');
            $this->call('UsersTableSeeder');
            $this->call('JobsCommentTableSeeder');


        Model::reguard();
    }
}
