### 1. Búa til verkefni og fake gögn (seeder classes) ###

#Terminal Skipun
composer create-project laravel/laravel IncrementalAPIs
// Býr til grunnverkefni
// ATH býr ekki til routes, t.d. fyrir auth og password reset.

#Terminal
php -S localhost:8888 -t public
// Keyrir verkefni á porti 8888

Installed 
https://github.com/laracasts/Laravel-5-Generators-Extended

### Tables

## Jobs
id          ->
job         -> string
responseble -> string (User)
price       -> decimal
Status      -> string
comment     -> text

## JobsComment
id          -> 
jobID       -> integer
comment     -> text
userID      -> integer

## User
Already exists in the laravel project


#Terminal
php artisan make:migration:schema create_jobs_table --schema="job:string, responseble:string, price:decimal, status:string, commentFinance:text"

php artisan make:migration:schema create_JobsComment_table --schema="jobID:integer:unsigned, userID:integer:unsigned, comment:text"

php artisan make:seed jobs

* Þurfti að breyta Klasanafninu kom bara {{class}}
* Þurfti að bæta við use App\Lesson í bæði JobsTableSeeder og DatabaseSeeder og use Faker\Factory as Faker; í JobsTableSeeder.
* þurfti að eiga við bæði JobsTableSeeder og DatabaseSeeder.

#Terminal
composer dump-autoload
php artisan db:seed

Bjó til UserTableSeeder skjal og bjó til tvo notendur, uppfærði DatabaseSeeder.

#Terminal
composer dump-autoload
php artisan db:seed

Bjó til JobsCommentsTableSeeder skjal og uppfærði DatabaseSeeder.

#Terminal
composer dump-autoload
php artisan migrate:refresh --seed

###  Grunn dummy gögn ættu að vera tilbúinn ###

### 2. Routes and Controller (starting with jobs)

Added routes recource to JobsController and a prefix api/v1 to all routes on it.

#Terminal
php artisan make:controller JobsController
php artisan route:list 

Worked on methods index() and show($id)
Created transformCollection and transform method and extracted them to a new class with new namespace.

Change composer.json add "app/Acme" under autoload->classmap.

#Terminal
composer dump
// to generate new autoload files

remember to add namespaces to new files and constructor and 

------

Create API Controller and put in it error message return methods

