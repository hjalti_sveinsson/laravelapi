<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Job;
use Response;

//use Acme\Transformers;
//use Acme\Transformers\Transformer;
use Acme\Transformers\JobTransformer;


class JobsController extends ApiController
{

    protected $jobTransformer;

    function __construct(JobTransformer $jobTransformer)
    {
        $this->jobTransformer = $jobTransformer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $allJobs = Job::all();

        return $this->respond([
            'data' => $this->jobTransformer->transformCollection($allJobs->toArray())
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $job = Job::find($id);

        if (!$job)
        {
            return $this->respondNotFound();
        }

        return $this->respond([$this->jobTransformer->transform($job)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
