<?php 

namespace Acme\Transformers;

class JobTransformer extends Transformer {

    public function transform($job)
    {
        return [
            'id'            => $job['id'],
            'address'       => $job['job'],
            'responsible'   => $job['responsible'],
            'total_price'   => $job['price'],
            'status'        => $job['status'],
            'comment_charge'=> $job['commentFinance'],
            'active'        => (boolean)$job['active'],
            'created_at'    => $job['created_at'],
            'updated_at'    => $job['updated_at']

        ];
    }
}