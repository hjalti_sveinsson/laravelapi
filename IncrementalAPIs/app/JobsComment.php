<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobsComment extends Model
{
    protected $fillable = [
      'jobID', 
      'userID',
      'comment'
    ];
}
